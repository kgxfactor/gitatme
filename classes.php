<?php


class subject {

/* Put all properties here */	
	protected $firstname;
	protected $lastname;
	protected $age;
	protected $income;
	
/* Let's do setters for everything; this is the input side of things, 
 * and we know we're going to use input. It would be equally acceptable
 * to put the properties near their functions as well */	
	
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
	}
	
	 
	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
	
	
	public function setAge($age) {
		$this->age = $age;
	}
	
	public function setIncome($income) {
		$this->income = $income;
	}

	

/* Getters, and "getter-like" things for output here. We don't want to
 * be making all our business public, so not everything will get 
 * a plain getter */
 
/* this is okay as a getter */

	public function getFirstname() {
            return $this->firstname;
	}
		
	public function getLastname() {
            return $this->lastname;
        }
        
        public function getAge() {
            return $this->age;
        }
        
        public function getIncome() {
            return $this->income;
        }
        
        
/* Modified getter here -- let's NEVER let the user see the last name without the DR attached. */
	public function getSpecialLastname() {
		$drblank = "Dr. " . $this->lastname;
		return $drblank; 
	}
	
        
        
/* AGE GOES IN, COLOR COMES OUT. NO exceptions. No returning the age 
 * Note, even these cascading ifs are a bit sloppy. Two things
 * one could do to make this better is to use better "between" syntax,
 * as well as introduce some error checking. */
  	
	public function agetocolor() {

		$theage = $this->age;

		if($theage <= 20) {
			$color = "Blue";
			}   
		else if ($theage <= 40) {
			$color = "Red";
			}			
		else if ($theage <= 60) {
			$color = "Green";
			}
		else  {$color="Yellow";}  /* no upper bound to age?*/ 
	   
		return $color;
        }

/* ditto on income and food - compact style */
		public function incometofood() {
	
				$theincome = $this->income; 
			
				if ($theincome <= 10000) {$food="Biscuit";}
				else if ($theincome <= 50000) {$food="Muffin";}
				else if ($theincome <= 150000) {$food="Toast";}
				else {$food="Cronut";} /* ditto on upper bound to income? */	
				
				return $food;
			   
        }
}

