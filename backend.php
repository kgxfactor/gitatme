<?php

/* This code is INCOMPLETE!
 *This is what I have changed
 * It should begin to read the file, but it will not output anything, at least yet; that is part of your assignment.
 *
 */


require_once('./classes.php');

$storage_file = "./storage.txt";

unset($file_handle);

$csvfile = fopen($storage_file,'r');

while(!feof($csvfile)) {

	    $allfields[] = fgetcsv($csvfile);
	       
}
  
/* as we saw at the end of class, the $allfields variable above is an array composed of arrays.  Specifically, each array within the bigger array is an array composed of each of the four elements in the csv file (storage.txt) in order.  
 *
 * As such, you should now create an array of OBJECTS, of the class type "subject" (which is the class we have invented for this exercise) So, again, it should be an array of objects, composed of ALL of the objects in our "database" (which, in this case, is simply a flat-file)
 */

$subject_array = [];

foreach ($allfields as $row){
$currsub = new subject();

$currsub->setFirstname($row[0]);
$currsub->setLastname($row[1]);
$currsub->setAge($row[2]);
$currsub->setIncome($row[3]);

$subject_array[] = $currsub;
}

/* NEXT, after that, create a well formed "report" that returns the following information for EVERY object in the "database," some
thing like
 
 * Subject Name: Firstname Lastname
 * Age: age
 * Income: Income
 * Subject Code : color food
 *
 * Hopefully this is where you begin to see the possibe advantages of OOP; here we can very effectively reuse the code we've already created to provide the subject code (and note, the subject code is not actually stored in the database, and if the coding methodology changes, we're still good) 
 */

$record_number = 1;
foreach ($subject_array as $currsub){
    echo "<br>";
    echo"<h3> Record Number $record_number</h3>";
    echo"<h4> Subject Name: ";
    echo$currsub->getFirstname();
    echo" ";
    echo$currsub->getLastname();
    echo"</h4>";
    echo "<strong>SUBJECT CODE:</strong> " .$currsub->agetocolor() . " " .$currsub->
    incometofood();
    
    $record_number ++;
}